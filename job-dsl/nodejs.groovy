job('NodeJS example') {
    scm {
        git('https://gitlab.com/learning-jenkins/wardviaene-docker-demo.git') {  node -> // is hudson.plugins.git.GitSCM
            node / gitConfigName('Erwin Kodiat')
            node / gitConfigEmail('jenkins-dsl@newtech.academy')
        }
    }
    triggers {
        scm('H/5 * * * *')
    }
    wrappers {
        nodejs('nodejs') // this is the name of the NodeJS installation in 
                         // Manage Jenkins -> Configure Tools -> NodeJS Installations -> Name
    }
    steps {
        shell("npm install")
    }
}
